use clap::ArgMatches;
use std::path::Path;
use bio::alphabets::dna::n_alphabet;
use bio::data_structures::bwt;
use bio::data_structures::bwt::{bwt,less,Occ};
use bio::data_structures::fmindex::{FMIndex,FMIndexable};
use bio::data_structures::suffix_array::suffix_array;
use std::collections::HashSet;
use bio::io;

pub fn main(args:&ArgMatches){
    let filepath = Path::new(args.value_of("INPUT").unwrap());
    let k = args.value_of("K").unwrap();
    if !filepath.exists(){
        eprintln!("Error: file {:?} does not exists.",filepath);
    }
    let res = match k.parse::<u64>(){
        Ok(res) => repeat_analysis(filepath, res),
        Err(why) => {eprintln!("{:?}",why);return},
    };
    match res {
        Ok(res) => print_result(&res),
        Err(why) => eprintln!("{:?}",why)
    };
}

fn print_result(res:&[(Vec<u8>,f64,f64)]){
    for (kmer, null_dist, emprical) in res{
        let kmer = String::from_utf8_lossy(kmer);
        println!("{}\t{}\t{}",kmer, null_dist, emprical);
    }
}

fn open_file(filepath:&Path)-> std::io::Result<Vec<u8>>{
    let extension = filepath.extension().map(|e|e.to_str().unwrap_or(""))
        .unwrap_or(&"");
    if extension.ends_with("a"){
        Ok(io::fasta::Reader::from_file(&filepath)?
           .records()
           .filter_map(|e|e.ok())
           .fold(vec![],|mut acc,record|{
               acc.extend(record.seq());
               acc
           }))
    }else if extension.ends_with("q"){
        let mut genome = io::fastq::Reader::from_file(&filepath)?
        .records()
            .filter_map(|e|e.ok())
            .fold(vec![],|mut acc:Vec<u8>, record|{
                acc.extend(record.seq());
                acc});
        genome.iter_mut().for_each(|e|e.make_ascii_uppercase());
        genome.push('$' as u8);
        Ok(genome)
    }else{
        eprintln!("Could not determine the file type of {:?}.", filepath);
        eprintln!("Please check the extension of the file");
        Err(std::io::Error::from(std::io::ErrorKind::InvalidData))
    }
}

struct RepeatAnalyzer{
    fm:FMIndex<bwt::BWT,bwt::Less,bwt::Occ>,
    length:usize,
}

impl RepeatAnalyzer{
    fn new(genome:& [u8])->Self{
        let alphabet = n_alphabet();
        let sa = suffix_array(&genome);
        let bwt = bwt(&genome,&sa);
        let less = less(&bwt,&alphabet);
        let occ = Occ::new(&bwt,3,&alphabet);
        let fm = FMIndex::new(bwt,less,occ);
        let length = genome.len() - 1;// For sentinel
        RepeatAnalyzer{
            fm,
            length
        }
    }
    fn count(&self,kmer:&[u8])-> usize{
        let interval = self.fm.backward_search(kmer.iter());
        interval.upper - interval.lower
    }
    fn ln_probability(&self,kmer:&[u8])-> f64{
        let count = self.count(kmer);
        let k = kmer.len();
        (count as f64).ln() - ((self.length - k + 1) as f64).ln()
    }
    fn ln_predict(&self,kmer:&[u8]) -> f64{
        let k = kmer.len();
        let partition:Vec<f64> = (1..k as usize).map(|i|{
            let ln_prob_first_half =  self.ln_probability(&kmer[..i]);
            let ln_prob_last_half = self.ln_probability(&kmer[i..]);
            ln_prob_first_half + ln_prob_last_half
        }).collect();
        Self::logsumexp(&partition) - ((k-1) as f64).ln()
    }
    fn is_significant(&self,kmer:&[u8],threshold:f64)->bool{
        self.ln_predict(kmer) - self.ln_probability(kmer) > threshold.ln()
    }
    fn logsumexp(data:&[f64])->f64{
        use std::f64;
        // eprintln!("Expected:{}",data.iter().map(|e|e.exp()).fold(0.,|acc,x|x+acc).ln());
        let max = data.iter().fold(f64::MIN,|max,&x| if max < x { x }else{ max });
        max + data.iter().map(|x| (x-max).exp()).fold(0.,|acc,x| acc + x).ln()
    }
}

/// This function would calculate how many repeat is there in a given fasta/fastq file.
/// the threshold to assert that the repeat is "frequent" is 30X likely.
fn repeat_analysis(filepath:&Path, k:u64)->std::io::Result<Vec<(Vec<u8>,f64,f64)>>{
    let genome = open_file(filepath)?;
    let repeat_analyzer  = RepeatAnalyzer::new(&genome);
    let mut kmers_so_far = HashSet::new();
    let mut res = vec![];
    for kmer in genome[..genome.len()-1].windows(k as usize){
        if kmers_so_far.contains(kmer){
            continue;
        }
        kmers_so_far.insert(kmer);
        if repeat_analyzer.is_significant(kmer,30.0){
            res.push((kmer.to_vec(),repeat_analyzer.ln_predict(kmer),
                      repeat_analyzer.ln_probability(kmer)));
        }
    }
    Ok(res)
}

#[cfg(test)]
mod test{
    use rand::thread_rng;
    use rand::Rng;
    use super::*;
    const EPSILON:f64 = 1e-5f64;
    fn small()->Vec<u8>{
        b"AATTAATTCC$".to_vec()
    }
    fn small2()->Vec<u8>{
        b"CAGTGCATGCATGCTGATCGTAGCATGTGCAGCATGTGCATGCATCGATCGAT$".to_vec()
    }
    fn long()->Vec<u8>{
        let bases = b"ACGT".to_vec();
        let mut rng = thread_rng();
        let mut r:Vec<_> = (0..10000).filter_map(|_| rng.choose(&bases))
            .map(|&e|e)
            .collect();
        r.push(b'$');
        r
    }
    #[test]
    fn construct_test(){
        let genome = small();
        let _test = RepeatAnalyzer::new(&genome);
    }
    fn diff(x:f64,y:f64)->f64{
        (x-y).abs()
    }
    fn check(x:f64,y:f64)->bool{
        diff(x,y) < EPSILON
    }
    #[test]
    fn empirical_test_1mer(){
        let genome = small();
        let ra = RepeatAnalyzer::new(&genome);
        let a = ra.ln_probability(&['A' as u8]).exp();
        debug_assert!(check(a,0.4),"{}",a);
        let t = ra.ln_probability(&['T' as u8]).exp();
        debug_assert!(check(t,0.4),"{}",t);
        let c = ra.ln_probability(&['C' as u8]).exp();
        debug_assert!(check(c,0.2),"{}",c);
        let g = ra.ln_probability(&['G' as u8]).exp();
        debug_assert!(g < EPSILON,"{}",g);
    }
    #[test]
    fn empirical_test_2mer(){
        let genome = small();
        let ra = RepeatAnalyzer::new(&genome);
        for k in 1..3{
            let k_mers:Vec<_> = genome[..genome.len()-1].windows(k).collect();
            let total = k_mers.len() as f64;
            for k_mer in &k_mers {
            let prob = ra.ln_probability(k_mer).exp();
            let prob_exact = k_mers.iter()
                .filter(|&e| e == k_mer).count() as f64 / total;
                debug_assert!(check(prob,prob_exact),"{:?}",k_mer);
            }
        }
    }
    #[test]
    fn empirical_test_small2(){
        let genome = small2();
        let ra = RepeatAnalyzer::new(&genome);
        for k in 1..8{
            let k_mers:Vec<_> = genome[..genome.len()-1].windows(k).collect();
            let total = k_mers.len() as f64;
            for k_mer in &k_mers {
            let prob = ra.ln_probability(k_mer).exp();
            let prob_exact = k_mers.iter()
                .filter(|&e| e == k_mer).count() as f64 / total;
                debug_assert!(check(prob,prob_exact),"{:?}",k_mer);
            }
        }
    }
    //#[test]
    fn empirical_test_long(){
        let genome = long();
        let ra = RepeatAnalyzer::new(&genome);
        for k in 3..10{
            let k_mers:Vec<_> = genome[..genome.len()-1].windows(k).collect();
            let total = k_mers.len() as f64;
            for k_mer in &k_mers {
            let prob = ra.ln_probability(k_mer).exp();
            let prob_exact = k_mers.iter()
                .filter(|&e| e == k_mer).count() as f64 / total;
                debug_assert!(check(prob,prob_exact),"{:?}",k_mer);
            }
        }
    }
 //   b"AATTAATTCC$".to_vec()
    #[test]
    fn predict_test_small(){
        let genome = small();
        let ra = RepeatAnalyzer::new(&genome);
        let prob = ra.ln_predict(b"AA").exp();
        debug_assert!(check(prob,4.*4./100.),"{}",prob);
        let prob = ra.ln_predict(b"TA").exp();
        debug_assert!(check(prob,4.*4./100.),"{}",prob);
        let prob = ra.ln_predict(b"GA").exp();
        debug_assert!(prob < EPSILON,"{}",prob);
        let prob = ra.ln_predict(b"TC").exp();
        debug_assert!(check(prob,4.*2./100.),"{}",prob);
    }
    #[test]
    fn predict_test_small2(){
        let genome = small();
        let ra = RepeatAnalyzer::new(&genome);
        let prob = ra.ln_predict(b"AAA").exp();
        let exact = (2./9. * 4./10. + 4./10. * 2./9.)/2.;
        debug_assert!(check(prob,exact),"{} vs {}",prob,exact);
        let prob = ra.ln_predict(b"TAT").exp();
        let exact = (1./9. * 4./10. + 4./10. * 2./9.)/2.;
        debug_assert!(check(prob,exact),"{}",prob);
        let prob = ra.ln_predict(b"GAC").exp();
        debug_assert!(prob < EPSILON,"{}",prob);
        let prob = ra.ln_predict(b"TCA").exp();
        let exact = (1./9. * 4./10. + 4./10. * 0.)/2.;
        debug_assert!(check(prob,exact),"{}",prob);
    }
}

