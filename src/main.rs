extern crate bio;
extern crate rust_htslib;
extern crate clap;
extern crate rayon;
extern crate regex;
extern crate rand;
#[macro_use] extern crate lazy_static;
mod mapping_stat;
mod repeat_analysis;
mod sequence_statistics;
mod sam;
use clap::{App,Arg,SubCommand};

fn repeat_analysis()->App<'static, 'static>{
    SubCommand::with_name("repeat_analysis")
        .author("Bansho Masutani")
        .about("A tiny tool to speculate how many repeat are there in genome")
        .version("0.1")
        .arg(Arg::with_name("INPUT")
             .short("i")
             .required(true)
             .long("input")
             .takes_value(true)
             .value_name("FASTA/FASTQ")
             .help("path/to/input. The format would be automatically detected."))
        .arg(Arg::with_name("K")
             .short("k")
             .required(false)
             .default_value("10")
             .long("kmer")
             .help("The kmer-cutoff.")
             .takes_value(true))
}

fn sequence_statistics()->App<'static, 'static>{
    SubCommand::with_name("sequence_statistics")
        .author("BanshoMasutani")
        .about("A tiny tool to summarize sequences")
        .version("0.1")
        .arg(Arg::with_name("INPUT")
             .short("i")
             .required(true)
             .long("input")
             .takes_value(true)
             .multiple(true)
             .last(true)
             .value_name("FASTA/FASTQ")
             .help("/path/to/fasta|fastq. Multiple arrowed. Format automatically detected."))
        .arg(Arg::with_name("OUTFORMAT")
             .short("o")
             .required(false)
             .long("format")
             .takes_value(true)
             .default_value("tsv")
             .value_name("FORMAT")
             .possible_values(&["tsv","latex","csv"])
             .help("output format. One of {tsv, latex, csv}"))
        .arg(Arg::with_name("THREADS")
             .short("t")
             .long("threads")
             .takes_value(true)
             .default_value("1")
             .help("The number of threads to be used."))
}

fn bam_statistics()->App<'static, 'static>{
    SubCommand::with_name("bam_statistics")
        .author("BanshoMasutani")
        .about("A tiny tool to summarize mappings")
        .version("0.1")
        .arg(Arg::with_name("INPUT")
             .short("i")
             .required(true)
             .long("input")
             .takes_value(true)
             .multiple(true)
             .last(true)
             .value_name("BAM")
             .help("/path/to/input. Multiple arrowed. Bam files should be indexed. To summarize sam, use sam_statistics."))
        .arg(Arg::with_name("OUTFORMAT")
             .short("o")
             .required(false)
             .long("format")
             .takes_value(true)
             .default_value("tsv")
             .value_name("FORMAT")
             .possible_values(&["tsv","latex","csv"])
             .help("output format. One of {tsv, latex, csv}"))
        .arg(Arg::with_name("THREADS")
             .short("t")
             .long("threads")
             .takes_value(true)
             .default_value("1")
             .help("The number of threads to be used."))

}

fn sam_statistics()->App<'static, 'static>{
    SubCommand::with_name("sam_statistics")
        .author("BanshoMasutani")
        .about("A tiny tool to summarize mappings")
        .version("0.1")
        .arg(Arg::with_name("INPUT")
             .short("i")
             .required(true)
             .long("input")
             .takes_value(true)
             .multiple(true)
             .last(true)
             .value_name("SAM")
             .help("/path/to/input. Multiple arrowed. To summarize bam, use bam_statistics."))
        .arg(Arg::with_name("OUTFORMAT")
             .short("o")
             .required(false)
             .long("format")
             .takes_value(true)
             .default_value("tsv")
             .value_name("FORMAT")
             .possible_values(&["tsv","latex","csv"])
             .help("output format. One of {tsv, latex, csv}"))
        .arg(Arg::with_name("THREADS")
             .short("t")
             .long("threads")
             .takes_value(true)
             .default_value("1")
             .help("The number of threads to be used."))

}


fn main() {
    let subcommands = vec![repeat_analysis(),
                           sequence_statistics(),
                           bam_statistics(),
                           sam_statistics()];
    let com_matches = subcommands.into_iter()
        .fold(clap::App::new("BBB")
              .version("0.1")
              .author("Bansho Masutani")
              .about("Tiny daily tools"),
              |app,subcom|app.subcommand(subcom))
        .get_matches();
    if let Some(matches) = com_matches.subcommand_matches("repeat_analysis"){
        repeat_analysis::main(matches);
        return;
    }
    if let Some(matches) = com_matches.subcommand_matches("sequence_statistics"){
        sequence_statistics::main(matches);
        return ;
    }
    if let Some(matches) = com_matches.subcommand_matches("bam_statistics"){
        mapping_stat::bam_statistics(matches);
        return;
    }
    if let Some(matches) = com_matches.subcommand_matches("sam_statistics"){
        mapping_stat::sam_statistics(matches);
        return;
    }
}
