extern crate rand;
extern crate bio;
extern crate regex;
extern crate rayon;
extern crate rust_htslib;
#[macro_use] extern crate lazy_static;
pub mod sam;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
