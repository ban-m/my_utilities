use super::sam;
use clap::ArgMatches;
use rayon::prelude::*;
use rust_htslib::bam;
use rust_htslib::bam::Read;

/// Note: there are ambiguity depends on how we should tead multimaps.
/// One way is to ingore secondary alignments, just use the "would-be-best"
/// alignment.
/// I take this way, because if I employ more loose criteria such as regarding all
/// alignments as different read segment, it would create overestimation of mismatch,
/// Indel rate, and total mapped bases.
/// The reason why such anomary occurs is that, when there are many sequences similar to
/// each other, they contribute certain amount of mismatches each other.
/// As a criteria of uniqueness, I use the second field of SAM/BAM record, namely, FALG.
/// FLAG && 0x900 == 0.
/// Also, note that the
/// Total length: including  clippings. excluding unmapped reads.
/// Bases mapped: including only mapped segment,i.e., ignoring unmapped reads,
///               soft/hard clips.
/// [Mismatch|Insertion|Deletion] rate: The fraction of the type of the edit operation in ALL ALIGNMENT operation.
struct Statistics {
    name: String,
    raw_total_sequence: u64,
    reads_mapped: u64,
    reads_unmapped: u64,
    reads_mapq0: u64,
    total_length: u64,
    bases_mapped: u64,
    bases_matched: u64,
    bases_unmatched: u64,
    average_length: u64,
    max_length: u64,
    insertion_bases: u64,
    deletion_bases: u64,
    mismatch_rate: f64,
    insertion_rate: f64,
    deletion_rate: f64,
}

impl Statistics {
    fn print(&self, delim: &str) {
        let record = [
            self.name.clone(),
            format!("{}", self.raw_total_sequence),
            format!("{}", self.reads_mapped),
            format!("{}", self.reads_unmapped),
            format!("{}", self.reads_mapq0),
            format!("{}", self.total_length),
            format!("{}", self.bases_mapped),
            format!("{}", self.bases_matched),
            format!("{}", self.bases_unmatched),
            format!("{}", self.average_length),
            format!("{}", self.max_length),
            format!("{}", self.insertion_bases),
            format!("{}", self.deletion_bases),
            format!("{}", self.mismatch_rate),
            format!("{}", self.insertion_rate),
            format!("{}", self.deletion_rate),
        ]
        .join(delim);
        println!("{}", record);
    }
}

#[derive(Debug)]
struct DraftStat {
    name: String,
    not_have_nm_tag: bool,
    raw_total_sequence: u64,
    reads_mapped: u64,
    reads_mapq0: u64,
    total_length: u64,
    total_alignment_operation: u64,
    bases_mapped: u64,
    bases_matched: u64,
    bases_unmatched: u64,
    max_length: u64,
    insertion_bases: u64,
    deletion_bases: u64,
}

impl DraftStat {
    fn with_name(name: &str) -> Self {
        let not_have_nm_tag = false;
        let name = name.to_string();
        let raw_total_sequence = 0;
        let reads_mapped = 0;
        let reads_mapq0 = 0;
        let total_length = 0;
        let total_alignment_operation = 0;
        let bases_mapped = 0;
        let bases_matched = 0;
        let bases_unmatched = 0;
        let max_length = 0;
        let insertion_bases = 0;
        let deletion_bases = 0;
        DraftStat {
            name,
            not_have_nm_tag,
            raw_total_sequence,
            reads_mapped,
            reads_mapq0,
            total_length,
            total_alignment_operation,
            bases_mapped,
            bases_matched,
            bases_unmatched,
            max_length,
            insertion_bases,
            deletion_bases,
        }
    }
    
    fn summarize_bam_cigar(record:&bam::Record)->(u64,u64,u64,u64,u64,u64,bool){
        //(length, align, mapped, matched, unmatched, ins, del, has_nm_tag)
        use self::bam::record::Cigar;
        use self::bam::record::Aux;
        let nm_tag = record.aux(&['N' as u8,'M' as u8]);
        let has_nm_tag = nm_tag.is_some();
        let (length, aln, ins, del) = record.cigar().iter()
            .fold((0,0,0,0),|(len,aln,ins,del),op|{
                match op{
                    Cigar::Match(l) | Cigar::Equal(l) | Cigar::Diff(l) => (len+l,aln+l,ins,del),
                    Cigar::Del(l) => (len,aln+l,ins,del+l),
                    Cigar::Ins(l) => (len+l,aln+l,ins+l,del),
                    Cigar::HardClip(l) | Cigar::SoftClip(l) => (len+l,aln,ins,del),
                    _  => (len,aln,ins,del),
                }
            });
        let (length, aln, ins, del) = (length as u64, aln as u64, ins as u64, del as u64);
        if has_nm_tag{
            match nm_tag.unwrap(){
                Aux::Integer(nm) => {
                    let nm = nm as u64;
                    (length, aln, aln - nm, nm - ins - del, ins, del, has_nm_tag)
                },
                _ => (length, aln, aln - ins - del, 0, ins, del, has_nm_tag)
            }
        }else{
            (length, aln, aln - ins - del, 0, ins, del, has_nm_tag)
        }
    }

    fn summarize_sam_cigar(record:&sam::Record)->(u64,u64,u64,u64,u64,u64,bool){
        //(length, align, mapped, matched, unmatched, ins, del, has_nm_tag)
        use self::sam::Op;
        let nm_tag = record.attr().iter().filter(|tag|tag[0] == "NM").nth(0);
        let has_nm_tag = nm_tag.is_some();
        let (length, aln, ins, del) = record.cigar_ref()
            .iter()
            .fold((0,0,0,0),|(len,aln,ins,del),op|{
                match op{
                    Op::Align(l) | Op::Match(l) | Op::Mismatch(l) => (len+l,aln+l,ins,del),
                    Op::Deletion(l) => (len,aln+l,ins,del+l),
                    Op::Insertion(l) => (len+l,aln+l,ins+l,del),
                    Op::HardClip(l) | Op::SoftClip(l) => (len+l,aln,ins,del),
                    _  => (len,aln,ins,del),
                }
            });
        let (length, aln, ins, del) = (length as u64, aln as u64, ins as u64, del as u64);
        if has_nm_tag{
            if let Ok(nm) = nm_tag.unwrap()[2].parse::<u64>(){
                (length, aln, aln - nm, nm - ins - del, ins, del, has_nm_tag)
            }else{
                (length, aln, aln - ins - del, 0, ins, del, has_nm_tag)
            }
        }else{
            (length, aln, aln - ins - del, 0, ins, del, has_nm_tag)
        }
    }

    
    fn update_sam(&mut self, record: &sam::Record) {
        if !record.is_primary() {
            return;
        }
        self.raw_total_sequence += 1;
        if record.flag() & 0x4 != 0{
            return ;
        }
        self.reads_mapped += 1;
        if record.mapq() == 0{
            self.reads_mapq0 += 1;
            return;
        }
        let (length, align, matched, unmatched, ins, del, has_nm_tag) = Self::summarize_sam_cigar(&record);
        let mapped = align - del;
        self.not_have_nm_tag = self.not_have_nm_tag | !has_nm_tag;
        self.max_length = self.max_length.max(length);
        self.total_length += length;
        self.total_alignment_operation += align;
        self.bases_mapped += mapped;
        self.bases_matched += matched;
        self.bases_unmatched += unmatched;
        self.insertion_bases += ins;
        self.deletion_bases += del;
    }
    fn update_bam(&mut self, record: &bam::Record) {
        let flag = record.flags();
        if flag & 0x900 != 0{
            return;
        }
        self.raw_total_sequence += 1;
        if flag & 0x4 != 0 {
            return;
        }
        self.reads_mapped +=1;
        if record.mapq() == 0{
            self.reads_mapq0 += 1;
            return;
        }
        let (length, align,  matched, unmatched, ins,del, has_nm_tag) = Self::summarize_bam_cigar(record);
        let mapped = align - del;
        self.not_have_nm_tag = self.not_have_nm_tag | !has_nm_tag;
        self.max_length = self.max_length.max(length);
        self.total_length += length;
        self.total_alignment_operation += align;
        self.bases_mapped += mapped;
        self.bases_matched += matched;
        self.bases_unmatched += unmatched;
        self.insertion_bases += ins;
        self.deletion_bases += del;
    }
    fn finalize(self) -> Statistics {
        if !self.not_have_nm_tag {
            eprintln!("Warning: at least one record does not have NM tag.");
            eprintln!("The output summary may be inaccurate.");
            eprintln!("For fix this, please first `samtools calmd` to calculate MN tag");
        }
        Statistics {
            name: self.name,
            raw_total_sequence: self.raw_total_sequence,
            reads_mapped: self.reads_mapped,
            reads_unmapped: self.raw_total_sequence - self.reads_mapped,
            reads_mapq0: self.reads_mapq0,
            total_length: self.total_length,
            bases_mapped: self.bases_mapped,
            bases_matched: self.bases_matched,
            bases_unmatched: self.bases_unmatched,
            average_length: self.total_length / self.reads_mapped,
            max_length: self.max_length,
            insertion_bases: self.insertion_bases,
            deletion_bases: self.deletion_bases,
            mismatch_rate: self.bases_unmatched as f64 / self.total_alignment_operation as f64,
            insertion_rate: self.insertion_bases as f64 / self.total_alignment_operation as f64,
            deletion_rate: self.deletion_bases as f64 / self.total_alignment_operation as f64,
        }
    }
}

pub fn sam_statistics(args: &ArgMatches) {
    let files: Vec<&str> = args.values_of("INPUT").unwrap().collect();
    let format = args.value_of("OUTFORMAT").unwrap();
    let threads: usize = match args.value_of("THREADS").unwrap().parse() {
        Ok(res) => res,
        Err(why) => {
            eprintln!("{:?}", why);
            return;
        }
    };
    rayon::ThreadPoolBuilder::new()
        .num_threads(threads)
        .build_global()
        .unwrap();
    let result: Vec<_> = files
        .into_par_iter()
        .filter_map(|e| summarize_sam(e))
        .collect();
    match format {
        "tsv" => print_delim(result, "\t"),
        "csv" => print_delim(result, ","),
        "latex" => print_latex(result),
        _ => unreachable!(),
    };
}

pub fn bam_statistics(args: &ArgMatches) {
    let files: Vec<&str> = args.values_of("INPUT").unwrap().collect();
    let format = args.value_of("OUTFORMAT").unwrap();
    let threads: usize = match args.value_of("THREADS").unwrap().parse() {
        Ok(res) => res,
        Err(why) => {
            eprintln!("{:?}", why);
            return;
        }
    };
    rayon::ThreadPoolBuilder::new()
        .num_threads(threads)
        .build_global()
        .unwrap();
    let result: Vec<_> = files
        .into_par_iter()
        .filter_map(|e| summarize_bam(e))
        .collect();
    match format {
        "tsv" => print_delim(result, "\t"),
        "csv" => print_delim(result, ","),
        "latex" => print_latex(result),
        _ => unreachable!(),
    };
}
use std::path::Path;
fn validate_path<'a>(file: &'a str) -> Option<(&'a Path, String)> {
    let file = Path::new(file);
    let name = file.file_stem()?.to_str()?;
    if !file.exists() {
        eprintln!("file:{:?} does not exists", file);
        None
    } else {
        Some((file, name.to_string()))
    }
}

fn summarize_sam(file: &str) -> Option<Statistics> {
    let (path, name) = validate_path(file)?;
    let mut record = sam::Record::new();
    let mut reader = sam::Reader::new(std::fs::File::open(path).ok()?);
    let mut stat = DraftStat::with_name(&name);
    while let Ok(bytes) = reader.read(&mut record) {
        if bytes == 0 {
            break;
        }
        // Summarize;
        stat.update_sam(&record);
    }
    Some(stat.finalize())
}

fn summarize_bam(file: &str) -> Option<Statistics> {
    let (path, name) = validate_path(file)?;
    let mut reader = bam::IndexedReader::from_path(path).ok()?;
    let mut record = bam::Record::new();
    let mut stat = DraftStat::with_name(&name);
    loop {
        match reader.read(&mut record) {
            Ok(_) => stat.update_bam(&record),
            Err(bam::ReadError::NoMoreRecord) => break,
            Err(why) => {
                eprintln!("{:?}", why);
                break;
            }
        }
    }
    Some(stat.finalize())
}

fn print_delim(result: Vec<Statistics>, delim: &str) {
    let header = [
        "name",
        "raw total sequence",
        "reads mapped",
        "reads unmapped",
        "reads mapq0",
        "total length",
        "bases mapped",
        "bases matched",
        "bases unmatched",
        "average length",
        "max length",
        "insertion bases",
        "deletion bases",
        "mismatch rate",
        "insertion rate",
        "deletion rate",
    ]
    .join(delim);
    println!("{}", header);
    for record in result {
        record.print(delim);
    }
}

fn print_latex(result: Vec<Statistics>) {
    let header = [
        "name",
        "raw total sequence",
        "reads mapped",
        "reads unmapped",
        "reads mapq0",
        "total length",
        "bases mapped",
        "bases matched",
        "bases unmatched",
        "average length",
        "max length",
        "insertion bases",
        "deletion bases",
        "mismatch rate",
        "insertion rate",
        "deletion rate",
    ];
    let align: String = header
        .iter()
        .enumerate()
        .map(|(idx, _)| if idx == 0 { 'r' } else { 'l' })
        .collect();
    println!("\\begin{{tabular}}{{ {} }} \\\\ \\toprule", align);
    println!("{} \\\\ \\midrule", header.join("&"));
    for record in result {
        record.print("&");
    }
    println!("\\end{{tabular}}");
}
