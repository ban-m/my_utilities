#![allow(dead_code)]
use std::slice;
use std::fs::File;
use std::io;
use std::io::{BufRead,BufReader};
use regex::Regex;
use std::path::Path;
use rayon::prelude::*;
use std::cmp::max;

pub fn into_coverage(reads:&[Record])->Vec<Coverage>{
    reads.par_iter().fold(||vec![],|acc,sam|combine_sam(acc,sam))
        .reduce(||vec![],|a,b|merge_two_coverages(a,b))
}


#[derive(Debug,Clone)]
pub struct Coverage{
    r_name:String,
    cov:Vec<(usize,u64)>
}

fn merge_two_coverages(a:Vec<Coverage>,b:Vec<Coverage>)->Vec<Coverage>{
    let mut res = Vec::with_capacity(a.len().max(b.len()));
    let mut aiter = a.into_iter().peekable();
    let mut biter = b.into_iter().peekable();
    while let (Some(arname),Some(brname)) = (aiter.peek().map(|e|e.r_name.clone())
                                             ,biter.peek().map(|e|e.r_name.clone())){
        if arname < brname {
            res.push(aiter.next().unwrap().clone());
        }else if arname > brname{
            res.push(biter.next().unwrap().clone());
        }else{
            let acov = aiter.next().unwrap();
            let bcov = biter.next().unwrap();
            res.push(acov.merge(&bcov).clone());
        }
    }
    res.extend(aiter);
    res.extend(biter);
    res
}


impl Coverage{
    pub fn r_name(&self)->&str{
        &self.r_name
    }
    pub fn get_cov(&self)->slice::Iter<(usize,u64)>{
        self.cov.iter()
    }

    fn merge(&self,cov:&Self)->Self{
        let mut res = Vec::with_capacity(max(self.cov.len(),cov.cov.len()));
        if self.r_name != cov.r_name{
            panic!("merging {:?},{:?}",self,cov);
        }else{
            let mut selfiter = self.cov.iter().peekable();
            let mut coviter = cov.cov.iter().peekable();
            while let (Some((s_index,_)),
                       Some((c_index,_))) = (selfiter.peek(),
                                                   coviter.peek()){
                if s_index < c_index {
                    res.push(selfiter.next().unwrap().clone())
                }else if s_index > c_index {
                    res.push(coviter.next().unwrap().clone())
                }else{
                    let (s_index,s_depth) = selfiter.next().unwrap();
                    let (_c_index,c_depth) = coviter.next().unwrap();
                    res.push((*s_index,s_depth+c_depth))
                }
            }
            res.extend(selfiter);
            res.extend(coviter);
        }
        Coverage{r_name:self.r_name.clone(),
                 cov:res}
    }
}

fn combine_sam(mut acc:Vec<Coverage>,sam:&Record)->Vec<Coverage>{
    match acc.binary_search_by(|probe|probe.r_name.cmp(&sam.r_name)){
        Err(index) => acc.insert(index,sam.to_coverage()),
        Ok(index) => acc[index] = acc[index].merge(&sam.to_coverage()),
    }
    acc
}

pub fn load_sam_file(file:&Path)->io::Result<Vec<Record>>{
    // annotation for retuened value:(b,cigar,pos) where,
    //b: true when the read mapped to template strand,
    // cigar: cigar string for the alignment,
    // pos: First position of matched base
    Ok(Reader::new(File::open(file)?).records()
       .filter_map(|e|e.ok())
       .collect())
}

#[derive(Debug)]
pub struct Reader<R: io::Read>{
    reader: io::BufReader<R>,
    buffer:String,
}

impl<R:io::Read> Reader<R>{
    pub fn new(reader:R)->Self{
        let buffer = String::with_capacity(10000);
        Reader{
            reader:BufReader::new(reader),
            buffer:buffer,
        }
    }
}

impl<R:io::Read> Reader<R>{
    pub fn read(&mut self,record:&mut Record)->std::io::Result<usize>{
        let bytes = loop{
            let bytes = self.reader.read_line(&mut self.buffer)?;
            if !self.buffer.starts_with("@"){
                break bytes;
            }
        };
        let err = || io::Error::from(io::ErrorKind::InvalidData);
        record.clear();
        for (index,rec) in self.buffer.splitn(12,'\t').enumerate(){
            match index {
                0 => record.q_name = rec.to_string(),
                1 => record.flag = rec.parse().map_err(|_|err())?,
                2 => record.r_name = rec.to_string(),
                3 => record.pos = rec.parse().map_err(|_|err())?,
                4 => record.mapq = rec.parse().map_err(|_|err())?,
                5 => record.cigar = parse_cigar_string(rec),
                6 => record.rnext = rec.to_string(),
                7 => record.pnext = rec.parse().map_err(|_|err())?,
                8 => record.tlen = rec.parse().map_err(|_|err())?,
                9 => record.seq = rec.to_string(),
                10 => record.qual = rec.chars()
                    .map(|e|e as u8 - 33)
                    .collect(),
                11 => record.attr = rec.split('\t')
                    .filter_map(|e| {
                        let mut e = e.split(':');
                        Some([e.next()?.to_string(),
                              e.next()?.to_string(),
                              e.next()?.to_string()])
                    })
                    .collect(),
                _ => {},
            };
        }
        Ok(bytes)
    }
    fn records(self)-> Records<R>{
        Records{
            reader:self,
            has_ended:false,
        }
    }
}

pub struct Records<R:io::Read>{
    reader: Reader<R>,
    has_ended:bool,
}

impl<R:io::Read> Iterator for Records<R>{
    type Item = io::Result<Record>;
    fn next(&mut self)->Option<Self::Item>{
        if self.has_ended{
            None
        }else{
            let mut record = Record::new();
            match self.reader.read(&mut record){
                Ok(res) if res == 0  || record.is_empty() => {
                    self.has_ended = true;
                    None
                },
                Ok(_) => {
                    Some(Ok(record))
                },
                Err(why) => {
                    self.has_ended = true;
                    Some(Err(why))
                },
            }
        }
    }
}

#[derive(Debug,Clone)]
pub struct Record{
    q_name:String,
    flag:u32,
    r_name:String,
    pos:usize,
    mapq:u8,
    cigar:Vec<Op>,
    rnext:String,
    pnext:usize,
    tlen:usize,
    seq:String,
    qual:Vec<u8>,
    attr:Vec<[String;3]>,
}

use std::fmt;
impl fmt::Display for Record{
    fn fmt(&self,f:&mut fmt::Formatter)->fmt::Result{
        let attr:String = self.attr.iter()
            .map(|tag| format!("{}:{}:{}",tag[0],tag[1],tag[2]))
            .collect();
        write!(f,"{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}",
               self.q_name,self.flag,self.r_name,self.pos,self.mapq,self.cigar_as_str(),
               self.rnext,self.pnext,self.tlen,self.seq,self.qual_as_str(),attr)
    }
}



impl Record{
    pub fn new()->Self{
        Record{
            q_name:String::new(),
            flag:0,
            r_name:String::new(),
            pos:0,
            mapq:0,
            cigar:Vec::new(),
            rnext:String::new(),
            pnext:0,
            tlen:0,
            seq:String::new(),
            qual:Vec::new(),
            attr:Vec::new(),
        }
    }
    fn clear(&mut self){
        self.q_name.clear();
        self.r_name.clear();
        self.cigar.clear();
    }
    fn is_empty(&self)->bool{
        self.q_name.is_empty() &&
            self.r_name.is_empty() &&
            self.cigar.is_empty()
    }
    pub fn q_name(&self)->&str{
        &self.q_name
    }
    pub fn r_name(&self)->&str{
        &self.r_name
    }
    pub fn ref_name(&self)->&str{
        &self.r_name
    }
    pub fn is_primary(&self)->bool{
        (self.flag & 0x900) == 0
    }
    pub fn is_template(&self)->bool{
        (self.flag  & 0b10000) == 0b10000
    }
    pub fn flag(&self) -> u32{
        self.flag
    }
    pub fn seq(&self)-> &str{
        &self.seq
    }
    pub fn pos(&self)->usize{
        self.pos
    }
    pub fn mapq(&self) -> u8{
        self.mapq
    }
    pub fn mapped_region(&self)->(usize,usize){
        use self::Op::*; // 0-BASED!!!!!
        let (head_clip, middle, _tail_clip, _) =
            self.cigar
            .iter()
            .fold((0, 0, 0, true), |acc, x| match x {
                HardClip(b) | SoftClip(b) if acc.3 => (acc.0 + b, acc.1, acc.2, acc.3),
                HardClip(b) | SoftClip(b) if !acc.3 => (acc.0, acc.1, acc.2 + b, acc.3),
                Align(b) | Insertion(b) | Match(b) | Mismatch(b) => {
                    (acc.0, acc.1 + b, acc.2, false)
                }
                _ => acc,
            });
        (head_clip, head_clip+middle)
    }
    pub fn get_range(&self)->(usize,usize){
        // Return the position of the genome(measured in template). 0-BASED!!!!
        let start = self.pos;
        if start == 0 {return (0,0)};
        use self::Op::*;
        let len:usize = self.cigar.iter()
            .map(|op|
                 match *op{
                     Align(b) | Match(b) | Deletion(b)|Skipped(b)|Mismatch(b) => b,
                     Insertion(_)|SoftClip(_)|HardClip(_)|Padding(_) => 0,
                 })
            .sum();
        (start-1,start+len-1)
    }
    pub fn query_length(&self)->usize{
        self.cigar().iter()
            .map(|e|match e {
                Op::HardClip(b) | Op::SoftClip(b) | Op::Align(b) | Op::Match(b) | Op::Mismatch(b) | Op::Insertion(b) => *b,
                _ => 0,
            }).sum()
    }
    pub fn cigar(&self)->Vec<Op>{
        self.cigar.clone()
    }
    pub fn cigar_ref(&self)->&[Op]{
        &self.cigar
    }
    pub fn to_coverage(&self)->Coverage{
        let mut cov = vec![];
        let mut start = self.pos;// reference position
        for op in &self.cigar{
            use self::Op::*;
            match *op{
                Align(b) | Match(b) => {
                    for i in 0..b{
                        cov.push((start+i,1));
                    }
                    start += b;
                }
                Insertion(_)|SoftClip(_)|HardClip(_)|Padding(_) => {},
                Deletion(b)|Skipped(b)|Mismatch(b) => {start +=b},
            }
        }
        Coverage{r_name:self.r_name.to_string(),
                 cov:cov}
    }
    fn cigar_as_str(&self)->String{
        let mut res = String::new();
        for op in &self.cigar{
            res.push_str(&op.as_str());
        }
        res
    }
    fn qual_as_str(&self)->String{
        self.qual.iter().map(|e|(e+33) as char).collect()
    }
    pub fn attr(&self)->&Vec<[String;3]>{
        &self.attr
    }
}

#[derive(Debug,Clone,Copy,Eq,PartialEq)]
pub enum Op{
    Align(usize),//M
    Insertion(usize),//I
    Deletion(usize),//D
    Skipped(usize),//N
    SoftClip(usize),//S
    HardClip(usize),//H
    Padding(usize),//P
    Match(usize),//=
    Mismatch(usize),//X
}

impl Op{
    fn new(op:&str)->Option<Op>{
        let (operation,num):(char,usize) = {
            let mut op = String::from(op);
            let operation = op.pop()?;
            (operation,op.parse().ok()?)
        };
        match operation{
            'M' => Some(Op::Align(num)),
            'I' => Some(Op::Insertion(num)),
            'D' => Some(Op::Deletion(num)),
            'N' => Some(Op::Skipped(num)),
            'S' => Some(Op::SoftClip(num)),
            'H' => Some(Op::HardClip(num)),
            'P' => Some(Op::Padding(num)),
            '=' => Some(Op::Match(num)),
            'X' => Some(Op::Mismatch(num)),
            _ => None
        }
    }
    fn as_str(&self)->String{
        let (num,op) = match self{
            Op::Align(x) => (x,'M'),
            Op::Insertion(x) => (x,'I'),
            Op::Deletion(x) => (x,'D'),
            Op::Skipped(x) => (x,'N'),
            Op::SoftClip(x) => (x,'S'),
            Op::HardClip(x) => (x,'H'),
            Op::Padding(x) => (x,'P'),
            Op::Match(x) => (x,'='),
            Op::Mismatch(x) => (x,'X'),
        };
        format!("{}{}",num,op)
    }
}



fn parse_cigar_string(cigar:&str)->Vec<Op>{
    lazy_static!{
        static ref RE:Regex = Regex::new(r"\d*[MIDNSHP=X]").unwrap();
    }
    RE.find_iter(cigar).filter_map(|e|Op::new(e.as_str())).collect()
}

#[test]
fn cigar_parse(){
    use super::sam::Op::*;
    let cigar = "101S33M2I66M";
    let processed = parse_cigar_string(&cigar);
    eprintln!("{:?}",processed);
    assert_eq!(processed,vec![SoftClip(101),
                    Align(33),
                    Insertion(2),
                    Align(66)]);
}

