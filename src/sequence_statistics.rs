use bio::io::fasta;
use bio::io::fastq;
use clap::ArgMatches;
use rayon::prelude::*;
use std::path::Path;
use std::time;
const DEBUG: bool = false;
#[derive(Debug)]
struct Statistics {
    name: String,
    total: usize,
    num_of_contig: usize,
    num_of_n: usize,
    num_of_gap: usize,
    mean_length: usize,
    ns: [usize; 5], //[n_10,n_30,n_50,n_70,n_90]
}

impl Statistics {
    fn print(&self, delim: &str) -> String {
        let ns: Vec<_> = self.ns.iter().map(|e| format!("{}", e)).collect();
        let ns = ns.join(delim);
        [
            self.name.clone(),
            format!("{}", self.total),
            format!("{}", self.mean_length),
            format!("{}", self.num_of_contig),
            ns,
            format!("{}", self.num_of_n),
            format!("{}", self.num_of_gap),
        ]
        .join(delim)
    }
}

pub fn main(args: &ArgMatches) {
    let start = time::Instant::now();
    let files: Vec<&str> = args.values_of("INPUT").unwrap().collect();
    let format = args.value_of("OUTFORMAT").unwrap();
    let threads: usize = match args.value_of("THREADS").unwrap().parse() {
        Ok(res) => res,
        Err(why) => {
            eprintln!("Invalid thread number");
            eprintln!("{}", why);
            return;
        }
    };
    rayon::ThreadPoolBuilder::new()
        .num_threads(threads)
        .build_global()
        .unwrap();
    let preprocess = time::Instant::now();
    let result: Vec<_> = files.into_par_iter().filter_map(|e| get_stats(e)).collect();
    match format {
        "tsv" => print_delim(result, "\t"),
        "csv" => print_delim(result, ","),
        "latex" => print_latex(result),
        _ => unreachable!(),
    };
    let end = time::Instant::now();
    if DEBUG {
        eprintln!("preprocess: {:?}", preprocess - start);
        eprintln!("end: {:?}", end - preprocess);
    }
}

fn print_delim(result: Vec<Statistics>, delim: &str) {
    let header = [
        "name",
        "total",
        "mean length",
        "# of contigs",
        "N10",
        "N30",
        "N50",
        "N70",
        "N90",
        "# of N",
        "# of gaps",
    ]
    .join(delim);
    println!("{}", header);
    for record in result {
        println!("{}", record.print(delim));
    }
}

fn print_latex(result: Vec<Statistics>) {
    let header = [
        "name",
        "total",
        "mean length",
        "\\# of contigs",
        "N10",
        "N30",
        "N50",
        "N70",
        "N90",
        "\\# of N",
        "\\# of gaps",
    ];
    let align: String = header
        .iter()
        .enumerate()
        .map(|(idx, _)| if idx == 0 { 'r' } else { 'l' })
        .collect();
    println!("\\begin{{tabular}}{{ {} }} \\\\ \\toprule", align);
    println!("{} \\\\ \\midrule", header.join("&"));
    for record in result {
        record.print("&");
    }
    println!("\\end{{tabular}}");
}

fn get_stats(path: &str) -> Option<Statistics> {
    let path = Path::new(path);
    let stem = path.file_stem()?.to_str()?;
    let extension = path.extension()?.to_str()?;
    // eprintln!("{}",extension);
    if extension.ends_with("a") {
        let reader = match fasta::Reader::from_file(path) {
            Ok(res) => Some(res),
            Err(why) => {
                println!("{:?}", why);
                return None;
            }
        }?;
        Some(calc_stats_fasta(reader, stem))
    } else if extension.ends_with("q") {
        let reader = match fastq::Reader::from_file(path) {
            Ok(res) => Some(res),
            Err(why) => {
                println!("{:?}", why);
                return None;
            }
        }?;
        Some(calc_stats_fastq(reader, stem))
    } else {
        eprintln!("Failed to detect filetype:{:?}", path);
        eprintln!("Pleaes specify *.fasta,*.fastq,*.fa,*.fq");
        None
    }
}

use bio::io::fasta::FastaRead;
use bio::io::fastq::FastqRead;
use std::io::Read;
fn calc_stats_fasta<R>(mut reader: fasta::Reader<R>, stem: &str) -> Statistics
where
    R: Read,
{
    let name = stem.to_string();
    let mut record = fasta::Record::new();
    let mut only_lengths = vec![];
    let (mut num_of_gap, mut num_of_n) = (0, 0);
    loop {
        reader.read(&mut record).unwrap();
        if record.is_empty() {
            break;
        };
        only_lengths.push(record.seq().len());
        let (gaps, n) = num_of_gap_and_n(record.seq());
        num_of_gap += gaps + 1;
        num_of_n += n;
    }
    let (total, ns, num_of_contig, mean_length) = length_stats(only_lengths);
    let num_of_gap = num_of_gap - 1;
    Statistics {
        name,
        total,
        num_of_contig,
        num_of_n,
        num_of_gap,
        mean_length,
        ns,
    }
}

fn calc_stats_fastq<R>(mut reader: fastq::Reader<R>, stem: &str) -> Statistics
where
    R: Read,
{
    let start = time::Instant::now();
    let name = stem.to_string();
    let mut record = fastq::Record::new();
    let mut only_lengths = Vec::with_capacity(1000000);
    let (mut num_of_gap, mut num_of_n) = (0, 0);
    loop {
        reader.read(&mut record).unwrap();
        if record.id().len() == 0{
            break;
        };
        only_lengths.push(record.seq().len());
        let (gaps, n) = num_of_gap_and_n(record.seq());
        num_of_gap += gaps + 1;
        num_of_n += n;
    }
    let end_read = time::Instant::now();
    let (total, ns, num_of_contig, mean_length) = length_stats(only_lengths);
    let end_stat = time::Instant::now();
    if DEBUG {
        eprintln!("Stat read:{:?}", end_read - start);
        eprintln!("Stat stat:{:?}", end_stat - end_read);
    }
    let num_of_gap = num_of_gap - 1;
    Statistics {
        name,
        total,
        num_of_contig,
        num_of_n,
        num_of_gap,
        mean_length,
        ns,
    }
}

fn length_stats(mut records: Vec<usize>) -> (usize, [usize; 5], usize, usize) {
    records.sort();
    let total: usize = records.iter().sum();
    let mut ns = [0; 5];
    let mut sofar = 0;
    let mut aim = 0;
    let num_of_sequence = records.len();
    let mean_length = total / num_of_sequence;
    for record in records {
        sofar += record;
        if sofar > (2 * aim + 1) * total / 10 {
            ns[aim] = record;
            aim += 1;
        }
    }
    (total, ns, num_of_sequence, mean_length)
}

#[inline]
fn is_base(base: &u8) -> bool {
    match base {
        b'a' | b'A' | b't' | b'T' | b'g' | b'G' | b'c' | b'C' => true,
        _ => false,
    }
}

#[inline]
fn num_of_gap_and_n(seq: &[u8]) -> (usize, usize) {
    let (mut gaps, mut previous, mut num_of_n) = (0, false, 0);
    for base in seq {
        let current = is_base(base);
        if !current {
            num_of_n += 1;
            if previous {
                gaps += 1;
            }
        }
        previous = current;
    }
    (gaps, num_of_n)
}

#[test]
fn gaps() {
    let test: Vec<u8> = vec!['a', 'a', 'n', 'n', 't', 'n', 'n', 'c', 'n']
        .into_iter()
        .map(|e| e as u8)
        .collect();
    assert_eq!(num_of_gap_and_n(&test), (3, 5));
}
