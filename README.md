# My utilities for bioinformatics


## Meta data
Author: Bansho Masutani

Mail: ban-m@g.ecc.u-tokyo.ac.jp

LastModified:2019/02/12

## Description

Tiny tools to carry out basic bioinfomatics routines such as serveying a genome, summrizing a bam/sam file, and so on.

Currently, the following tools available

- detection of exact repeats
- calculate statistics for given fasta/fastq files
- calculate statistics for given sam/bam files

## Requirement

- Rust langage
- libhts >= 1.8

## Install

After install Rust language, just type `cargo build --release`.
Then, all tools are in `./target/release/`

## Synopsis

All commands are distributed as a subcommand of `bbb [subcomamnds]`, and
if you type `bbb [subcommands] -h`, it would output a help menu for that command.


## Output format

You can configure the output format. Just adding `-o [tsv|csv|latex]` at the tail of the command would work.